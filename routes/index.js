var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res) {
  var room = null;
  res.render('index', {
    title: '두둥실 - 가볍게 메시지를 띄우다',
    room: room
  });
});

router.get('/:room', function(req, res) {
  var room = req.params.room;
  res.render('index', {
    title: '두둥실 - 가볍게 메시지를 띄우다',
    room: room
  });
});

module.exports = router;

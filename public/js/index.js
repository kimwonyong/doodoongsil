(function() {
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBpG7DwfMX1A8AWoB5UplGXtJmJfXwRJcg",
    authDomain: "doodoongsil-babe0.firebaseapp.com",
    databaseURL: "https://doodoongsil-babe0.firebaseio.com",
    projectId: "doodoongsil-babe0",
    storageBucket: "doodoongsil-babe0.appspot.com",
    messagingSenderId: "732962310767"
  };
  firebase.initializeApp(config);

  function Msg(msg) {
    this.msg = msg;
    this.color = randomColor({
      luminosity: 'light'
    });
  }

  var app = new Vue({
    el: '#app',
    data: {
      msg: null,
      msgArr: [
        new Msg('Loading...')
      ],
      room: $('#app').data('room'),
      isJoin: false,
      isClick: false
    },
    mounted: function() {
      var that = this;
      $('.modal').modal({
        dismissible: false
      });
      firebase.database().ref(".info/connected").on("value", function(snap) {
        if (snap.val() === true) { // firebase.database 연결되면
          if (that.room) {
            that.joinRoom();
          } else {
            $('#modal-room').modal('open');
          }
        } else {
        }
      });
    },
    methods: {
      goFreeboard: function() {
        var that = this;
        that.room = '자유게시판';
        that.joinRoom();
      },
      makeRoom: function() {
        var that = this;
        var room = that.room;
        that.isClick = true;
        if (room.length > 10) {
          that.isClick = false;
          Materialize.toast('잘못된 방이름입니다.', 2000);
          $('#modal-alert').modal('close');
          $('#modal-room').modal('open');
        } else {
          var newMsg = new Msg('여기는 "' + room + '" 방입니다.');
          firebase.database().ref('room/' + room).push(newMsg);
          that.joinRoom();
        }
      },
      joinRoom: function() {
        var that = this;
        var room = that.room;
        that.isClick = true;

        firebase.database().ref('room').once('value', function(snapshot) {
          var hasChild = snapshot.hasChild(room);
          if (hasChild) { // 방이 존재하면
            that.msgArr = [];
            that.isJoin = true;
            history.replaceState(null, null, room);
            firebase.database().ref('room/' + room).limitToLast(20).on('child_added', function(snapshot) {
              if (that.msgArr.length > 20) {
                that.msgArr.pop();
              }
              that.msgArr.unshift(snapshot.val());
            });
            $('.modal').modal('close');
          } else { // 없으면
            // 만들어야지
            that.isClick = false;
            $('#modal-alert').modal('open');
          }
        });
      },
      sendMsg: function() {
        var that = this;
        var room = that.room;
        var newMsg = new Msg(that.msg);
        firebase.database().ref('room/' + room).push(newMsg);
        that.msg = '';
      }
    },
    directives: {
      msg: {
        bind: function(el, binding, vnode) {
          var ww = $(window).width();
          var wh = $(window).height();

          var rw = anime.random(ww / 2 - 100, ww / 2 + 100);
          var rh = anime.random(wh / 2 - 100, wh / 2 + 100);

          $(el).addClass('animated flash');
          $(el).css("left", rw + "px");
          $(el).css("top", rh + "px");
        },
        inserted: function(el, binding, vnode) {
          (function msgEnterAnime() {
            var ww = $(window).width() / 2;
            var wh = $(window).height() / 2;

            var rw = anime.random(-ww, ww);
            var rh = anime.random(-wh, wh);

            var time = Math.sqrt(Math.pow(rw, 2) + Math.pow(rh, 2)) * 15;

            anime({
              targets: el,
              translateX: rw,
              translateY: rh,
              duration: time,
              easing: 'linear',
              complete: function() {
                msgEnterAnime();
              }
            });
          })();
        }
      }
    }
  });
})();
